<?php 

/*
	REPETITION CONTROL STRUCTURES

		-While Loop 
		-Do-while Loop
		-For Loop
*/

// WHILE LOOP
function whileLoop() {
	$count = 5;

	while($count !== 0) {
		echo $count.'<br/>';
		$count--;
	}
}


// DO-WHILE LOOP
function doWhileLoop() {
	$count = 20;

	do{
		echo $count.'<br/>';
		$count--;
	} 
	while($count > 0);
}


// FOR LOOP
function forLoop() {
	for($count = 0; $count <= 20; $count++) {
		echo $count.'<br/>';
	}
}

//============================================

/*
	CONTINUE and BREAK STATEMENTS

		Continue

			is a keyword that allows the code to go to the next loop without finishing the current code block.

		Break

			ends the execution of the current loop.

*/

function modifiedForLoop() {
	for($count = 0; $count <= 20; $count++) {
		if($count % 2 === 0) {
			continue;
		}
		echo $count.'<br/>';
		if($count > 10) {
			break;
		}
	}
}

//============================================

/*
	ARRAY MANIPULATION
*/

// before PHP v5.4.
$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927');

// Introduced on PHP v5.4 and up.
$studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];


// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'HP', 'Toshiba', 'Fujitsu'];
$task = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];


// Associative Array
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-Dimensional Array
$heroes = [
	['iron man', 'thor', 'hulk'],
	['wolverine', 'cyclope', 'storm'],
	['Darna', 'Captain Barbel', 'Lastikman']
];


// Two-Dimensional Associative Array
$ironManPowers = [
	'regular' => ['repulsor blast', 'rocket punch'],
	'signature' => ['unibeam']
];

// Array Sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;


// Sort Arrays
sort($sortedBrands);

// Reverse Sort
rsort($reverseSortedBrands);

// Other Array Function

	//				  (Array,   Element)
function searchBrands($brands, $brand){
	// 				(what,   where)
	return(in_array($brand, $brands)) ? "$brand is in the array" : "$brand is not in the array.";
}

$reverseGradePeriods = array_reverse($gradePeriods);
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity</title>
</head>
<body>

<!-- Loop -->
<h1>Loop</h1>
<?php divisibleByFive(); ?>


<!-- Array Manipulation -->
<h1>Array Mannipulation</h1>

<pre><?php print_r($students) ?></pre>

<pre><?php echo count($students); ?></pre>

<?php array_push($students, 'Meg') ?>
<pre><?php print_r($students) ?></pre>

<pre><?php echo count($students); ?></pre>

<?php array_shift($students) ?>
<pre><?php print_r($students) ?></pre>
<pre><?php echo count($students); ?></pre>

</body>
</html>